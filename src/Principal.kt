fun main(){

    var num1:Double=0.0
    var num2:Double=0.0
    var result:Double=0.0
    var option:Int=1

    println("Write number 1 and press Enter")
    num1=readLine()!!.toDouble()

    println("Write number 2 and press Enter")
    num2=readLine()!!.toDouble()
    while (option!=0) {

        println("Choose one of the following options")
        println("1.Add the numbers")
        println("2.Substract the numbers")
        println("3.Multiply the numbers")
        println("4.Divide the numbers")
        println("5.Power of number one with number two")
        println("6.Square root of number one")
        println("0.Exit")
        option = readLine()!!.toInt()


        if (option == 1) {
            result=sumar(num1,num2)
        }
        else if (option == 2) {
            result =restar(num1,num2)
        }
        else if (option == 3) {
            result =multiplicar(num1,num2)
        }
        else if (option == 4) {
            result =dividir(num1,num2)
        }
        else if (option == 5) {
            result =potencia(num1,num2)
        }
        else if (option == 6) {
            result =raíz(num1)
        }
        if (option>=1&& option<=6){
            println("THe result is: " + result)
        }
        else {
            println("Goodbye")
        }
    }
}

fun sumar(n1:Double,n2:Double):Double{
    var res:Double=n1+n2
    return res
}
fun restar(n1:Double,n2:Double):Double{
    var res:Double=n1-n2
    return res
}
fun multiplicar(n1:Double,n2:Double):Double{
    var res:Double=n1*n2
    return res
}
fun dividir(n1:Double,n2:Double):Double{
    var res:Double=n1/n2
    return res
}
fun potencia(n1:Double,n2:Double):Double{
    var res:Double=Math.pow(n1,n2)
    return res
}
fun raíz(n1:Double):Double{
    var res:Double=Math.sqrt(n1)
    return res
}

